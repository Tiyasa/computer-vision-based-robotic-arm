#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>   // Header files of opencv, includes opencv features in the program
#include<iostream>

#include <string>

 using namespace cv;
 using namespace std;

 int main(int argc, char const *argv[])
 {
 	cv::namedWindow("captured", cv::WINDOW_AUTOSIZE );

 	cv::VideoCapture cap;
 	if(argc==1){
 		cap.open(0);
 	}
 	else{                             // open the camera
 		cap.open(argv[1]);
 	}
 	if(!cap.isOpened()){
 										//Check if we succeedeed
 			
 		std::cerr << "Couldn't open capture."<<std::endl;
 		return -1;
 	}
 		cv::Mat frame;
 		
 			cap >> frame;                          // captures image in matrix frame

 			cv::imwrite("imagecaptured.jpg",frame );  // stores captured image as imagecaptured.jpg in the same repository.






 	return 0;
 }