#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include<iostream>

#include <string>

 using namespace cv;
 using namespace std;

 int main(int argc, char const *argv[])
 {


 		cv::VideoCapture left;
 		cv::VideoCapture right;

 		if(argc==1){
 		left.open(0);
 		right.open(1);
 	}
 	else{
 		left.open(argv[1]);
 		right.open(argv[1]);
 	}
 	if(!left.isOpened()){
 		std::cerr << "Couldn't open left capture."<<std::endl;
 	}

 	if(!right.isOpened()){
 		std::cerr << "Couldn't open  right capture."<<std::endl;
 	}
 		cv::Mat leftframe;
 		left >> leftframe;

 		cv::Mat rightframe;
 		right >> rightframe;


 		cv::imwrite("leftimage.jpg",leftframe);
 		cv::imwrite("rightimage.jpg",rightframe);

 	return 0;
 }