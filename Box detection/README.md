## Object detection can be done by two ways in openCv

1. Tree- Based Object Detection Techniques.
2. Object Detection Using Support Vector Machines


### Tree-Based Object Detection Techniques

Cascades are classifier.Above code is Haar Cascade Object Detector.  
The sample input:  
![box.jpg](https://gitlab.com/SaiTejaMutchi/computer-vision-based-robotic-arm/-/raw/master/Box%20detection/box.jpg)  
Output from the model:  
![detected](https://gitlab.com/SaiTejaMutchi/computer-vision-based-robotic-arm/-/raw/master/Box%20detection/detected.jpg)
- This is just simple still need trained model for industrial images.And also other detection techniques are also to be tested