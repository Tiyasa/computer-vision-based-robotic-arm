#include "opencv2/objdetect.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <iostream>
using namespace std;
using namespace cv;

//Object detection  using Haar Cascade
 
void detectAndDraw(Mat frame,CascadeClassifier cascade){
Mat frame_gray;
cvtColor(frame,frame_gray,COLOR_BGR2GRAY); //converting into gray scale image
equalizeHist(frame_gray,frame_gray);
 
 std::vector<Rect>objects;
 cascade.detectMultiScale(frame_gray, objects, 2.35, 2, cv::CASCADE_DO_CANNY_PRUNING, cv::Size(30, 30));
 for (auto& det : objects)
 {
 	rectangle(frame,det,Scalar(255,255,0),2);
 	// result
namedWindow("result", WINDOW_NORMAL);
imshow("result",frame);
waitKey(0);
imwrite("detected.jpg",frame);
 

    //close all the opened windows
    destroyAllWindows();	
 }

}


int main() {

  const String cascade_name("cascade.xml");
  CascadeClassifier cascade;
  
  if (!cascade.load(cascade_name)) {
    cout << "Error loading cascade\n";
    return -1;
 	 }
  
  
  Mat frame;
  frame = imread("box.jpg");
    detectAndDraw(frame, cascade);

  return 0;
}